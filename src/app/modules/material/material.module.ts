import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatToolbarModule, MatSidenavModule, MatProgressSpinnerModule,
        MatIconModule, MatButtonToggleModule, MatCardModule, MatTabsModule, MatListModule,
        MatDialogModule, MatFormFieldModule, MatDatepickerModule, MatGridListModule,
        MatNativeDateModule, MatInputModule, MatSelectModule, MatStepperModule,
        MatAutocompleteModule, MatTableModule, MatSnackBarModule, MatDividerModule, MatExpansionModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ChartModule } from 'primeng/chart';

const Material = [
  BrowserAnimationsModule,
  CommonModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatProgressSpinnerModule,
  MatIconModule,
  MatButtonToggleModule,
  MatCardModule,
  MatTabsModule,
  MatListModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatDatepickerModule,
  MatGridListModule,
  MatNativeDateModule,
  MatSelectModule,
  MatStepperModule,
  MatAutocompleteModule,
  MatTableModule,
  MatSnackBarModule,
  MatDividerModule,
  MatExpansionModule,
  ChartModule
];

@NgModule({
  declarations: [],
  imports: [
    Material
  ],
  exports: [
    Material
  ]
})
export class MaterialModule { }
