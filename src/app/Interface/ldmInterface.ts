export class LdmInterface {
    public tar: string;
    public data: string;
    public des_lav: string;
    public costo: string;
    public aliq: string;
    public tot: string;
    public sign_ddl: string;
    public sign_aa: string;
  }
