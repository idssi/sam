export class SalInterface {
    public tariffa: string;
    public data: string;
    public indicazione: string;
    public quantita: string;
    public importoUnitario: string;
    public aliq: string;
    public tot: string;
  }
