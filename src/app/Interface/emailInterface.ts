export class EmailInterface {
    public data: string;
    public mittente: string;
    public destinatario: string[];
    public oggetto: string;
    public testo: string;
  }
