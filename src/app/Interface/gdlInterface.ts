export class GdlInterface {
    public data: string;
    public meteo: string;
    public annotazioni: string;
    public operai: string;
    public mezzi: string;
    public path_allegato: string;
    public approvato: string;
  }
