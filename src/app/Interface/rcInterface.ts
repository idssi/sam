export class RcInterface {
    public tariffa: string;
    public data: string;
    public indicazione: string;
    public percentuale: string;
    public prez_unit: string;
    public aliq: string;
    public debito: string;
    public tot: string;
    public sign_AA: string;
    public sign_DDL: string;
  }
