import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './modules/material/material.module';
import { HttpClientModule } from '@angular/common/http';

import { AuthService } from './services/auth/auth.service';
import { DlService } from './services/dl/dl.service';
import { RupService } from './services/rup/rup.service';
import { AaService } from './services/aa/aa.service';
import { UserService } from './services/user/user.service';

import { PublicHomeComponent } from './components/public/public-home/public-home.component';
import { CallbackComponent } from './components/_plus/callback/callback.component';
import { DlHomeComponent } from './components/subjects/dl/dl-home/dl-home.component';
import { RupHomeComponent } from './components/subjects/rup/rup-home/rup-home.component';
import { AaHomeComponent } from './components/subjects/aa/aa-home/aa-home.component';
import { MainComponent } from './components/public/main/main.component';
import { AppComponent } from './components/_plus/root/app.component';
import { GdlAddItemComponent } from './components/objects/gdl/gdl-add-item/gdl-add-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LdmAddItemComponent } from './components/objects/ldm/ldm-add-item/ldm-add-item.component';
import { RcAddItemComponent } from './components/objects/rc/rc-add-item/rc-add-item.component';
import { SalAddItemComponent } from './components/objects/sal/sal-add-item/sal-add-item.component';
import { MatPaginatorModule } from '@angular/material';
import { GdlVisualComponent } from './components/objects/gdl/gdl-visual/gdl-visual.component';
import { LdmVisualComponent } from './components/objects/ldm/ldm-visual/ldm-visual.component';
import { RcVisualComponent } from './components/objects/rc/rc-visual/rc-visual.component';
import { SalVisualComponent } from './components/objects/sal/sal-visual/sal-visual.component';
import { GdlInitInfoComponent } from './components/objects/gdl/gdl-init-info/gdl-init-info.component';
import { LdmInitInfoComponent } from './components/objects/ldm/ldm-init-info/ldm-init-info.component';
import { RcInitInfoComponent } from './components/objects/rc/rc-init-info/rc-init-info.component';
import { SalInitInfoComponent } from './components/objects/sal/sal-init-info/sal-init-info.component';
import { LoadingComponent } from './components/objects/loading/loading.component';
import { UserHomeComponent } from './components/subjects/user/user-home/user-home.component';
import { EmailComponent } from './components/objects/email/email.component';
import { GdlSignComponent } from './components/objects/gdl/gdl-sign/gdl-sign.component';
import { SalChartComponent } from './components/objects/sal/sal-chart/sal-chart.component';
import { LdmSignAaComponent } from './components/objects/ldm/ldm-sign-aa/ldm-sign-aa.component';
import { LdmSignDdlComponent } from './components/objects/ldm/ldm-sign-ddl/ldm-sign-ddl.component';
import { RcSignAaComponent } from './components/objects/rc/rc-sign-aa/rc-sign-aa.component';
import { RcSignDdlComponent } from './components/objects/rc/rc-sign-ddl/rc-sign-ddl.component';
import { SignErrorComponent } from './components/public/sign-error/sign-error.component';
@NgModule({
  declarations: [
    AppComponent,
    PublicHomeComponent,
    CallbackComponent,
    DlHomeComponent,
    RupHomeComponent,
    AaHomeComponent,
    MainComponent,
    GdlAddItemComponent,
    LdmAddItemComponent,
    RcAddItemComponent,
    SalAddItemComponent,
    GdlVisualComponent,
    LdmVisualComponent,
    RcVisualComponent,
    SalVisualComponent,
    GdlInitInfoComponent,
    LdmInitInfoComponent,
    RcInitInfoComponent,
    SalInitInfoComponent,
    LoadingComponent,
    UserHomeComponent,
    EmailComponent,
    GdlSignComponent,
    SalChartComponent,
    LdmSignAaComponent,
    LdmSignDdlComponent,
    RcSignAaComponent,
    RcSignDdlComponent,
    SignErrorComponent
  ],
  entryComponents: [
    GdlAddItemComponent,
    LdmAddItemComponent,
    RcAddItemComponent,
    SalAddItemComponent,
    GdlInitInfoComponent,
    LdmInitInfoComponent,
    RcInitInfoComponent,
    SalInitInfoComponent,
    LoadingComponent,
    EmailComponent,
    GdlSignComponent,
    SalChartComponent,
    LdmSignDdlComponent,
    LdmSignAaComponent,
    RcSignAaComponent,
    RcSignDdlComponent,
    SignErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule
  ],
  providers: [AuthService, DlService, RupService, AaService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
