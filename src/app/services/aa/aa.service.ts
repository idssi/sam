import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './../auth/auth.service';
import { map } from 'rxjs/operators';
import { EmailInterface } from 'src/app/Interface/emailInterface';


@Injectable()
export class AaService {

  API_URL = 'http://localhost:3000';

  constructor(public authService: AuthService, private http: HttpClient) { }

  // --------------- APPROVA LIBRETTO MISURA -------------------
  async signMisuraAA(index: number) {
    const res = await this.http.post(`${this.API_URL}/aa/signMisuraAA`, { 'index': index }, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }

  // --------------- APPROVA REGISTRO ------------------------
  async approvaRegistroAA(ID: number) {
    const res = await this.http.post(`${this.API_URL}/aa/approvaRegistroAA`, { 'index': ID }, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }

  // --------------- AGGIUNGE NUOVO LAVORO ---------------------
  async setLavoro(jsonSetLavoro) {
    const res = await this.http.post(`${this.API_URL}/aa/setLavoro`, jsonSetLavoro, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }

  // --------------- AGGIUNGE NUOVA EMAIL -----------------------
  setEmail(jsonEmail) {
    this.http.post(`${this.API_URL}/aa/setEmail`, jsonEmail, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).subscribe(
      data => console.log(data)
    );
  }

  // --------------- VISUALIZZA EMAIL -----------------------
  async getEmail() {
    const res = await this.http.get<EmailInterface[]>(`${this.API_URL}/aa/getEmail`, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }

    // --------------- FIRMA CONTRATTO GDL INIZIALE ----------
  setSignGDLInit() {
    const res = this.http.post(`${this.API_URL}/aa/setSignGDLInit`, {}, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }
}
