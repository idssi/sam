import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Observer } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import * as auth0 from 'auth0-js';
import * as jwt_decode from "jwt-decode";

import 'rxjs/add/observable/of';
import 'rxjs/add/observable/timer';


@Injectable()
export class AuthService {
  auth0 = new auth0.WebAuth({
    audience: 'http://localhost:3000',
    clientID: 'bdT8nvWao33Z22elZcSbjokWWDTWKpaB',
    domain: 'gwynbleidd.eu.auth0.com',
    responseType: 'token id_token',
    redirectUri: 'http://localhost:4200/callback',
    scope: 'openid profile'
  });

  private _idToken: string;
  private _accessToken: string;
  private _expiresAt: number;

  userProfile: any;
  refreshSubscription: any;
  observer: Observer<boolean>;
  ssoAuthComplete$: Observable<boolean> = new Observable(
    obs => (this.observer = obs)
  );

  constructor(public router: Router) {
    this._idToken = '';
    this._accessToken = '';
    this._expiresAt = 0;
  }

  get accessToken(): string {
    return this._accessToken;
  }

  get idToken(): string {
    return this._idToken;
  }

  get decAccessToken(): string {
    return jwt_decode(this._accessToken);
  }

  get decIdToken(): string {
    return jwt_decode(this._idToken);
  }

  get role(): string {
    return this.decAccessToken['https://SAMroles/user_authorization'].roles[0];
  }

  private authorization() {
    switch (this.role) {
      case 'DL': {
        this.router.navigate(['dl']);
        break;
      }
      case 'AA': {
        this.router.navigate(['aa']);
        break;
      }
      case 'RUP': {
        this.router.navigate(['rup']);
        break;
      }
      default : {
        this.router.navigate(['access_denied']);
      }
    }
  }

  public login(): void {
    this.auth0.authorize();
  }

  public handleAuthentication(): void {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.localLogin(authResult);
        // this.authorization();
      } else if (err) {
        this.router.navigate(['']);
        console.log(err);
        alert(`Error: ${err.error}. Check the console for further details.`);
      }
    });
  }

  public getProfile(cb): void {
    if (!this._accessToken) {
      throw new Error('Access token must exist to fetch profile');
    }

    const self = this;
    this.auth0.client.userInfo(this._accessToken, (err, profile) => {
      if (profile) {
        self.userProfile = profile;
      }
      cb(err, profile);
    });
  }

  private localLogin(authResult): void {
    // Set the time that the access token will expire at
    const expiresAt = authResult.expiresIn * 1000 + Date.now();
    this._accessToken = authResult.accessToken;
    this._idToken = authResult.idToken;
    this._expiresAt = expiresAt;

    this.scheduleRenewal();
  }

  public logout(): void {
    // Remove tokens and expiry time
    this._accessToken = '';
    this._idToken = '';
    this._expiresAt = 0;
    this.unscheduleRenewal();

    this.auth0.logout({
      returnTo: window.location.origin
    });
  }

  public isAuthenticated(): boolean {
    // Check whether the current time is past the
    // access token's expiry time
    return this._accessToken && Date.now() < this._expiresAt;
  }

  public renewTokens() {
    this.auth0.checkSession({},
      (err, result) => {
        if (err) {
          this.router.navigate(['']);
        } else {
          this.localLogin(result);
          this.observer.next(true);
          this.authorization();
        }
      }
    );
  }

  public scheduleRenewal() {
    if (!this.isAuthenticated()) return;
    this.unscheduleRenewal();

    const expiresAt = this._expiresAt;

    const source = Observable.of(expiresAt).pipe(mergeMap(expiresAt => {
      const now = Date.now();

      // Use the delay in a timer to
      // run the refresh at the proper time
      return Observable.timer(Math.max(1, expiresAt - now));
    }));

    // Once the delay time from above is
    // reached, get a new JWT and schedule
    // additional refreshes
    this.refreshSubscription = source.subscribe(() => {
      this.renewTokens();
      this.scheduleRenewal();
    });
  }

  public unscheduleRenewal() {
    if (!this.refreshSubscription) return;
    this.refreshSubscription.unsubscribe();
  }
}
