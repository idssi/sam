import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './../auth/auth.service';
import { EmailInterface } from 'src/app/Interface/emailInterface';

@Injectable()
export class RupService {

  API_URL = 'http://localhost:3000';

  constructor(public authService: AuthService, private http: HttpClient) { }

  // --------------- AGGIUNGE NUOVO LAVORO -----------------------
  async setLavoro(jsonSetLavoro) {
    const res = await this.http.post(`${this.API_URL}/rup/setLavoro`, jsonSetLavoro, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }

  // --------------- AGGIUNGE NUOVA EMAIL -----------------------
  setEmail(jsonEmail) {
    this.http.post(`${this.API_URL}/rup/setEmail`, jsonEmail, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).subscribe(
      data => console.log(data)
    );
  }

  // --------------- VISUALIZZA EMAIL -----------------------
  async getEmail() {
    const res = await this.http.get<EmailInterface[]>(`${this.API_URL}/rup/getEmail`, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }

  // --------------- FIRMA CONTRATTO GDL INIZIALE ----------
  async setSignGDLInit() {
    console.log(this.authService.accessToken);
    const res = await this.http.post(`${this.API_URL}/rup/setSignGDLInit`, {}, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }

  // --------------- FIRMA CONTRATTO LDM INIZIALE ----------
  setSignLDMInit() {
    const res = this.http.post(`${this.API_URL}/rup/setSignLDMInit`, {}, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }

  // --------------- FIRMA CONTRATTO RC INIZIALE ----------
  setSignRCInit() {
    const res = this.http.post(`${this.API_URL}/rup/setSignRCInit`, {}, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }
}
