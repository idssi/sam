import { TestBed } from '@angular/core/testing';

import { DlService } from './dl.service';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from '../auth/auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('DlService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [DlService, AuthService],
    imports: [RouterTestingModule, HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: DlService = TestBed.get(DlService);
    expect(service).toBeTruthy();
  });
});
