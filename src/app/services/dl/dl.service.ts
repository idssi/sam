import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './../auth/auth.service';
import { GdlInterface } from './../../Interface/gdlInterface'
import { Observable } from 'rxjs';
import { JsonPipe } from '@angular/common';
import { async } from '@angular/core/testing';
import { EmailInterface } from 'src/app/Interface/emailInterface';


@Injectable()
export class DlService {

  API_URL = 'http://localhost:3000';

  constructor(public authService: AuthService, private http: HttpClient) { }

  // --------------- AGGIUNGE NUOVO REGISTRO -------------------
  setRegistro(jsonSetRegistro) {
    this.http.post(`${this.API_URL}/dl/setRegistro`, jsonSetRegistro, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).subscribe(
      data => console.log(data)
    );
  }

  // --------------- AGGIUNGE NUOVA MISURA ---------------------
  async setMisura(jsonSetMisura) {
    const res = await this.http.post(`${this.API_URL}/dl/setMisura`, jsonSetMisura, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }

  // --------------- AGGIUNGE NUOVO SAL --------------------------
  setSal(jsonSetSal) {
    this.http.post(`${this.API_URL}/dl/setSal`, jsonSetSal, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).subscribe(
      data => console.log(data)
    );
  }

  // --------------- AGGIUNGE NUOVO LAVORO -----------------------
  async setLavoro(jsonSetLavoro) {
    const res = await this.http.post(`${this.API_URL}/dl/setLavoro`, jsonSetLavoro, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }

  // --------------- APPROVA REGISTRO ----------------------------
  async approvaRegistroDDL(ID: number) {
    const res = await this.http.post(`${this.API_URL}/dl/approvaRegistroDDL`, { 'index': ID }, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }

  // --------------- APPROVA LAVORO ------------------------------
  async approvaLavoro(ID: number) {
    const res = await this.http.post(`${this.API_URL}/dl/approvaLavoro`, { 'ID': ID }, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }

  // --------------- APPROVA LIBRETTO MISURA -------------------
  async signMisuraDdl(index: number) {
    const res = await this.http.post(`${this.API_URL}/dl/signMisuraDdl`, { 'index': index }, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }



  // --------------- AGGIUNGE NUOVA EMAIL -----------------------
  async setEmail(jsonEmail) {
    const res = await this.http.post(`${this.API_URL}/dl/setEmail`, jsonEmail, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }


  // --------------- VISUALIZZA EMAIL -----------------------
  async getEmail() {
    const res = await this.http.get<EmailInterface[]>(`${this.API_URL}/dl/getEmail`, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }

  // --------------- FIRMA CONTRATTO GDL INIZIALE ----------
 setSignGDLInit() {
   const res = this.http.post(`${this.API_URL}/dl/setSignGDLInit`, {}, {
      headers: new HttpHeaders().set(
        'Authorization', `Bearer ${this.authService.accessToken}`)
    }).toPromise();
    return res;
  }
}

