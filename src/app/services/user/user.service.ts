import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import Web3 from 'Web3';

const Sal = require('../../../../../sam_contracts/build/contracts/Sal.json');
const Ldm = require('../../../../../sam_contracts/build/contracts/Ldm.json');
const Rc = require('../../../../../sam_contracts/build/contracts/Rc.json');

import { GdlInterface } from './../../Interface/gdlInterface';
import { SalInterface } from './../../Interface/salInterface';
import { RcInterface } from './../../Interface/rcInterface';
import { LdmInterface } from './../../Interface/ldmInterface';
import { EmailInterface } from 'src/app/Interface/emailInterface';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  //------------ OTTIENE GIORNALE COMPLETO -------------------------- 
  getGiornale(): Observable<GdlInterface[]> {
    return this.http.get<GdlInterface[]>('http://localhost:3000/public/getGiornale')
  }

  //------------ OTTIENE SAL COMPLETO ------------------------------ 
  getSal(): Observable<SalInterface[]> {
    return this.http.get<SalInterface[]>('http://localhost:3000/public/getSal')
  }

  //------------ OTTIENE REGISTRO COMPLETO ------------------------- 
  getRegistri(): Observable<RcInterface[]> {
    return this.http.get<RcInterface[]>('http://localhost:3000/public/getRegistri')
  }

  //------------ OTTIENE LIBRETTO MISURE COMPLETO ------------------
  getLdm(): Observable<LdmInterface[]> {
    return this.http.get<LdmInterface[]>('http://localhost:3000/public/getAllLdm')
  }

  async getEventInstance(contract) {
    const provider = new Web3.providers.WebsocketProvider("ws://localhost:22003");
    const web3 = new Web3(provider);
    const networkId = await web3.eth.net.getId();
    const deployedNetwork = contract.networks[networkId];
    return (new web3.eth.Contract(contract.abi, deployedNetwork && deployedNetwork.address));
  }

  //------------ OTTIENE FIRMA GLD DI AA  --------------------------
  async getSignAAGDLInit() {
    const res = await this.http.get('http://localhost:3000/public/getSignAAGDLInit').toPromise();
    return res;
  }

  //------------ OTTIENE FIRMA GLD DI DL  --------------------------
  async getSignDLGDLInit() {
    const res = this.http.get('http://localhost:3000/public/getSignDLGDLInit').toPromise();
    return res;
  }

  //------------ OTTIENE FIRMA GLD DI GDL  -------------------------
  async getSignRUPGDLInit() {
    const res = this.http.get('http://localhost:3000/public/getSignRUPGDLInit').toPromise();
    return res;
  }

  //------------ OTTIENE FIRMA LDM DI RUP  -------------------------
  async getSignRUPLDMInit() {
    const res = this.http.get('http://localhost:3000/public/getSignRUPLDMInit').toPromise();
    return res;
  }

  //------------ OTTIENE FIRMA RC DI RUP  -------------------------
  async getSignRUPRCInit() {
    const res = this.http.get('http://localhost:3000/public/getSignRUPRCInit').toPromise();
    return res;
  }

  //------------ OTTIENE OGGETTO CONTRATTO  -------------------------
  getOggettoGDLInit() {
    return this.http.get('http://localhost:3000/public/getOggettoGDLInit');
  }

  //------------ OTTIENE COMMITTENTE CONTRATTO ----------------------
  getCommittenteGDLInit() {
    return this.http.get('http://localhost:3000/public/getCommittenteGDLInit');
  }

  //------------ OTTIENE IMPRESA CONTRATTO ----------------------
  getImpresaGDLInit() {
    return this.http.get('http://localhost:3000/public/getImpresaGDLInit');
  }

  //------------ OTTIENE PREZZO TOTALE CONTRATTO ----------------------
  gettotalProgettoInit() {
    return this.http.get('http://localhost:3000/public/gettotalProgettoInit');
  }
}
