import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AaHomeComponent } from './aa-home.component';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { MainComponent } from 'src/app/components/public/main/main.component';
import { AuthService } from 'src/app/services/auth/auth.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('AaHomeComponent', () => {
  let component: AaHomeComponent;
  let fixture: ComponentFixture<AaHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AaHomeComponent, MainComponent ],
      imports: [
        MaterialModule,
        RouterTestingModule
      ],
      providers: [
        AuthService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AaHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
