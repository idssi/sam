import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MatDialog } from '@angular/material';
import { GdlAddItemComponent } from 'src/app/components/objects/gdl/gdl-add-item/gdl-add-item.component';

@Component({
  selector: 'app-aa-home',
  templateUrl: './aa-home.component.html',
  styleUrls: ['./aa-home.component.css']
})
export class AaHomeComponent implements OnInit {

  constructor(public dialog: MatDialog, public authService: AuthService) { }

  ngOnInit() {
  }

  gdlAddItem() {
    this.dialog.open(GdlAddItemComponent);
  }

}
