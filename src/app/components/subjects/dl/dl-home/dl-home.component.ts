import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { GdlAddItemComponent } from 'src/app/components/objects/gdl/gdl-add-item/gdl-add-item.component';
import { LdmAddItemComponent } from 'src/app/components/objects/ldm/ldm-add-item/ldm-add-item.component';
import { RcAddItemComponent } from 'src/app/components/objects/rc/rc-add-item/rc-add-item.component';
import { SalAddItemComponent } from 'src/app/components/objects/sal/sal-add-item/sal-add-item.component';
import { MatDialog } from '@angular/material';
import { DlService } from 'src/app/services/dl/dl.service';

@Component({
  selector: 'app-dl-home',
  templateUrl: './dl-home.component.html',
  styleUrls: ['./dl-home.component.css']
})
export class DlHomeComponent implements OnInit {

  @Output() searchEvent = new EventEmitter<string>();

  constructor(public dialog: MatDialog, public authService: AuthService, public dlserve: DlService) {
    this.authService.ssoAuthComplete$.subscribe(status => {
      console.log(this.authService.accessToken);
    });
   }

  ngOnInit() {
    this.authService.ssoAuthComplete$.subscribe(status => {
      console.log(this.authService.accessToken);
    });
  }

  gdlAddItem() {
    this.dialog.open(GdlAddItemComponent);
  }


  ldmAddItem() {
    this.dialog.open(LdmAddItemComponent);
  }

  rcAddItem() {
    this.dialog.open(RcAddItemComponent);
  }

  salAddItem() {
    this.dialog.open(SalAddItemComponent);
  }

  applyFilter(filterValue: string) {
    this.searchEvent.emit(filterValue);
    console.log("DL HOME: " + filterValue);
  }

}
