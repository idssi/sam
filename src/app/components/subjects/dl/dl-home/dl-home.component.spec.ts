import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DlHomeComponent } from './dl-home.component';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { MainComponent } from 'src/app/components/public/main/main.component';
import { AuthService } from 'src/app/services/auth/auth.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('DlHomeComponent', () => {
  let component: DlHomeComponent;
  let fixture: ComponentFixture<DlHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DlHomeComponent,
        MainComponent
      ],
      imports: [
        MaterialModule,
        RouterTestingModule
      ],
      providers: [ AuthService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DlHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
