import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RupHomeComponent } from './rup-home.component';
import { MainComponent } from 'src/app/components/public/main/main.component';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from 'src/app/services/auth/auth.service';

describe('RupHomeComponent', () => {
  let component: RupHomeComponent;
  let fixture: ComponentFixture<RupHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RupHomeComponent,
        MainComponent
      ],
      imports: [
        MaterialModule,
        RouterTestingModule
      ],
      providers: [ AuthService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RupHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
