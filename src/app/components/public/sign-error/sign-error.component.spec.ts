import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignErrorComponent } from './sign-error.component';

describe('SignErrorComponent', () => {
  let component: SignErrorComponent;
  let fixture: ComponentFixture<SignErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
