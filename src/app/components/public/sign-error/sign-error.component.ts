import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user/user.service';
import { DlService } from 'src/app/services/dl/dl.service';
import { AaService } from 'src/app/services/aa/aa.service';
import { RupService } from 'src/app/services/rup/rup.service';
import { AuthService } from 'src/app/services/auth/auth.service';


const GdlInit = require('../../../../../../sam_contracts/build/contracts/GdlInit.json');
const LdmInit = require('../../../../../../sam_contracts/build/contracts/LdmInit.json');
const RcInit = require('../../../../../../sam_contracts/build/contracts/RcInit.json');


@Component({
  selector: 'app-sign-error',
  templateUrl: './sign-error.component.html',
  styleUrls: ['./sign-error.component.css']
})
export class SignErrorComponent implements OnInit {

  GDLsignDL;
  GDLsignRUP;
  GDLsignAA;
  LDMsignRUP;
  RCsignRUP;
  DLlog = true;
  AAlog = true;
  RUPlog = true;

  GDLsignMissing = "";
  LDMsignMissing = "";
  RCsignMissing = "";

  role = "";

  constructor(public authService: AuthService, public userService: UserService, public dlService: DlService, public aaService: AaService, public rupService: RupService) { }

  async ngOnInit() {

    this.signInitGdl();
    this.signInitLdm();
    this.signInitRc();

    this.role = window.location.pathname;

    switch (this.role) {
      case "/dl": {
        this.DLlog = false;
        this.AAlog = true;
        this.RUPlog = true;

        break;
      }
      case "/aa": {
        this.AAlog = false;
        this.DLlog = true;
        this.RUPlog = true;

        break;
      }
      case "/rup": {
        this.RUPlog = false;
        this.DLlog = true;
        this.AAlog = true;

        break;
      }
    }

    this.GDLsignDL = await this.userService.getSignDLGDLInit();
    console.log(await this.GDLsignDL);

    this.GDLsignRUP = await this.userService.getSignRUPGDLInit();
    console.log(await this.GDLsignRUP);

    this.GDLsignAA = await this.userService.getSignAAGDLInit();
    console.log(await this.GDLsignAA);

    this.LDMsignRUP = await this.userService.getSignRUPLDMInit();
    console.log(await this.LDMsignRUP);

    this.RCsignRUP = await this.userService.getSignRUPRCInit();
    console.log(await this.RCsignRUP);


    if (await !this.GDLsignDL || await !this.GDLsignAA || await !this.GDLsignRUP) { this.GDLsignMissing = 'Firme mancanti'; }
    else { this.GDLsignMissing = 'Firme apportate'; }

    if (await !this.LDMsignRUP) { this.LDMsignMissing = 'Firme mancanti'; }
    else { this.LDMsignMissing = 'Firme apportate'; }

    if (await !this.RCsignRUP) { this.RCsignMissing = 'Firme mancanti'; }
    else { this.RCsignMissing = 'Firme apportate'; }


  }


  setGDLsign() {

    switch (this.role) {
      case "/dl": {
        this.dlService.setSignGDLInit().then(res => {
          console.log(res);
        });
        break;
      }
      case "/aa": {
        this.aaService.setSignGDLInit().then(res => {
          console.log(res);
        });
        break;
      }
      case "/rup": {
        this.rupService.setSignGDLInit().then(res => {
          console.log(res);
        });
        break;
      }
    }

  }

  setLDMsign() {

    switch (this.role) {
      case "/rup": {
        this.rupService.setSignLDMInit().then(res => {
          console.log(res);
        });
        break;
      }
    }

  }

  setRCsign() {

    switch (this.role) {
      case "/rup": {
        this.rupService.setSignRCInit().then(res => {
          console.log(res);
        });
        break;
      }
    }

  }

  disableAnimation = true;
  ngAfterViewInit(): void {
    setTimeout(() => this.disableAnimation = false);
  }

  async signInitGdl() {
    const instanceGdlInit = await this.userService.getEventInstance(GdlInit);
    instanceGdlInit.once('hackSignRup', {}, (error, event) => {
      this.GDLsignRUP = true;
    });

    instanceGdlInit.once('hackSignDirettore', {}, (error, event) => {
      this.GDLsignDL = true;
    });

    instanceGdlInit.once('hackSignImpresa', {}, (error, event) => {
      this.GDLsignAA = true;
    });
  }

  async signInitLdm() {
    const instanceLdmInit = await this.userService.getEventInstance(LdmInit);
    instanceLdmInit.once('hackSignRup', {}, (error, event) => {
      this.LDMsignRUP = true;
    });
  }

  async signInitRc() {
    const instanceRcInit = await this.userService.getEventInstance(RcInit);
    instanceRcInit.once('hackSignRup', {}, (error, event) => {
      this.RCsignRUP = true;
    });
  }
}
