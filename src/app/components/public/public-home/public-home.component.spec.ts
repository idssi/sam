import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicHomeComponent } from './public-home.component';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { AuthService } from 'src/app/services/auth/auth.service';
import { RouterTestingModule } from '@angular/router/testing';
import { DlService } from 'src/app/services/dl/dl.service';

import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('PublicHomeComponent', () => {
  let component: PublicHomeComponent;
  let fixture: ComponentFixture<PublicHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicHomeComponent ],
      imports: [
        MaterialModule,
        RouterTestingModule,
        HttpClientTestingModule
       ],
      providers: [
        AuthService,
        DlService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
