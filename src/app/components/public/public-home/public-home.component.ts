import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DlService } from 'src/app/services/dl/dl.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-public-home',
  templateUrl: './public-home.component.html',
  styleUrls: ['./public-home.component.css'],
  providers: [AuthService, DlService]
})
export class PublicHomeComponent {

  constructor(public authService: AuthService, public dlservice: DlService, public router: Router) {}

  appalti() {
    this.router.navigate(['user']);
  }

}
