import { Component, OnInit } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { MatDialog } from '@angular/material';
import { SignErrorComponent } from '../sign-error/sign-error.component';


const GdlInit = require('../../../../../../sam_contracts/build/contracts/GdlInit.json');
const LdmInit = require('../../../../../../sam_contracts/build/contracts/LdmInit.json');
const RcInit = require('../../../../../../sam_contracts/build/contracts/RcInit.json');

@Component({
  selector: 'app-main',
  animations: [
    trigger('rotatedState', [
      state('default', style({ transform: 'rotate(0)' })),
      state('rotated', style({ transform: 'rotate(90deg)' })),
      transition('rotated => default', animate('300ms ease-out')),
      transition('default => rotated', animate('300ms ease-in'))
    ]),
  ],
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit {
  // Sidenav Open/Close
  opened = false;
  role: any = "Role";
  profile: any;
  state: string = 'default';
  loc = window.location.pathname;

  signGDLAA;
  signGDLDL;
  signGDLRUP;
  signLDMRUP;
  signRCRUP;

  constructor(public authService: AuthService, private router: Router, public userService: UserService, public dialog: MatDialog) {
    // this.getIdToken();
  }

  async ngOnInit() {
    this.signInitGdl();
    this.signInitLdm();
    this.signInitRc();

    this.authService.ssoAuthComplete$.subscribe(status => {
      this.profile = this.authService.decIdToken;
      console.log(this.authService.accessToken);
    });

    this.signGDLDL = await this.userService.getSignDLGDLInit();
    console.log(await this.signGDLDL);

    this.signGDLRUP = await this.userService.getSignRUPGDLInit();
    console.log(await this.signGDLRUP);

    this.signGDLAA = await this.userService.getSignAAGDLInit();
    console.log(await this.signGDLAA);

    this.signLDMRUP = await this.userService.getSignRUPLDMInit();
    console.log(await this.signLDMRUP);

    this.signRCRUP = await this.userService.getSignRUPRCInit();
    console.log(await this.signRCRUP);

    this.controlInit();

  }

  async controlInit() {

    if (!(await this.signGDLDL && await this.signGDLRUP && await this.signGDLAA && await this.signLDMRUP && await this.signRCRUP)) {
      console.log('sono dentro');
      setTimeout(() => this.dialog.open(SignErrorComponent, { disableClose: true }));
    }
  }

  async getIdToken() {
    this.profile = await this.authService.decIdToken;
  }

  rotate() {
    this.state = (this.state === 'default' ? 'rotated' : 'default');
  }

  isUsr() {
    if (this.loc === '/user') {
      return false;
    } else {
      return true;
    }
  }

  home() {
    this.router.navigate(['']);
  }

  async signInitGdl() {
    const instanceGdlInit = await this.userService.getEventInstance(GdlInit);
    instanceGdlInit.once('hackSignImpresa', {}, (error, event) => {
      this.signGDLAA = true;
      if (this.signGDLDL && this.signGDLRUP && this.signLDMRUP && this.signRCRUP) {
        this.dialog.closeAll();
      }
    });

    instanceGdlInit.once('hackSignRup', {}, (error, event) => {
      this.signGDLRUP = true;
      if (this.signGDLDL && this.signGDLAA && this.signLDMRUP && this.signRCRUP) {
        this.dialog.closeAll();
      }
    });

    instanceGdlInit.once('hackSignDirettore', {}, (error, event) => {
      this.signGDLDL = true;
      if (this.signGDLAA && this.signGDLRUP && this.signLDMRUP && this.signRCRUP) {
        this.dialog.closeAll();
      }
    });
  }

  async signInitLdm() {
    const instanceLdmInit = await this.userService.getEventInstance(LdmInit);
    instanceLdmInit.once('hackSignRup', {}, (error, event) => {
      this.signLDMRUP = true;
      if (this.signGDLDL && this.signGDLRUP && this.signGDLAA && this.signRCRUP) {
        this.dialog.closeAll();
      }
    });

  }

  async signInitRc() {
    const instanceRcInit = await this.userService.getEventInstance(RcInit);
    instanceRcInit.once('hackSignRup', {}, (error, event) => {
      this.signRCRUP = true;
      if (this.signGDLDL && this.signGDLRUP && this.signLDMRUP && this.signGDLAA) {
        this.dialog.closeAll();
      }
    });
  }

}
