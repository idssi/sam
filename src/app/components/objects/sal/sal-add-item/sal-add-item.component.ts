import { Component } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { SalInterface } from './../../../../Interface/salInterface';
import { MatDialog, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-sal-add-item',
  templateUrl: './sal-add-item.component.html',
  styleUrls: ['./sal-add-item.component.css']
})
export class SalAddItemComponent {
  firstForm: FormGroup;
  secondForm: FormGroup;
  newSalWork = new SalInterface();

  constructor(private formBuilder: FormBuilder, private dialog: MatDialog, private snack: MatSnackBar) {
    this.firstForm = this.formBuilder.group({
      num_tariffa: ['', [Validators.required]],
      lavoro: ['', [Validators.required]]
    });

    this.secondForm = this.formBuilder.group({
      quantita: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      importo: ['', [Validators.required, Validators.pattern("^[0-9]*$")]]
    });
   }

   sendNewSal() {
    console.log("Invio SAL");
    this.newSalWork.tariffa = this.firstForm.value.tariffa;
    this.newSalWork.data = this.firstForm.value.data;
    this.newSalWork.indicazione = this.firstForm.value.indicazione;
    this.newSalWork.quantita = this.firstForm.value.quantita;
    this.newSalWork.importoUnitario = this.firstForm.value.importoUnitario;
    this.newSalWork.aliq = this.firstForm.value.aliq;
    this.newSalWork.tot = this.firstForm.value.tot;
    console.log(this.newSalWork);
    this.dialog.closeAll();
    this.snack.open("Inserimento Avvenuto!", "OK", {
      duration: 5000,
    });
  }

}
