import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { SalAddItemComponent } from './sal-add-item.component';

describe('SalAddItemComponent', () => {
  let component: SalAddItemComponent;
  let fixture: ComponentFixture<SalAddItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalAddItemComponent ],
      imports: [
        MaterialModule,
        ReactiveFormsModule,
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalAddItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
