import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalChartComponent } from './sal-chart.component';

describe('SalChartComponent', () => {
  let component: SalChartComponent;
  let fixture: ComponentFixture<SalChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
