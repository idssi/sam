import { Component, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-sal-chart',
  templateUrl: './sal-chart.component.html',
  styleUrls: ['./sal-chart.component.css']
})
export class SalChartComponent implements OnInit {

  visualBar = false;
  visualPol = false;
  visualLine = false;
  chart = 'Bar Chart SAL';

  count = 0;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.visualBar = true;
    this.visualPol = false;
    this.visualLine = false;
    this.chart = 'Bar Chart SAL';
  }

  changeChart() {
    this.count++;
    if (this.count > 2) {
      this.count = 0;
    }
    switch (this.count) {
      case 0: {
        this.visualBar = true;
        this.visualLine = false;
        this.visualPol = false;
        this.chart = 'Bar Chart SAL';
        break;
      }
      case 1: {
        this.visualBar = false;
        this.visualLine = true;
        this.visualPol = false;
        this.chart = 'Line Chart SAL';
        break;
      }
      case 2: {
        this.visualBar = false;
        this.visualLine = false;
        this.visualPol = true;
        this.chart = 'Polar Chart SAL';
        break;
      }

    }

  }
}
