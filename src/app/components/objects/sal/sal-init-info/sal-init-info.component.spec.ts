import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalInitInfoComponent } from './sal-init-info.component';

describe('SalInitInfoComponent', () => {
  let component: SalInitInfoComponent;
  let fixture: ComponentFixture<SalInitInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalInitInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalInitInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
