import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-sal-init-info',
  templateUrl: './sal-init-info.component.html',
  styleUrls: ['./sal-init-info.component.css']
})
export class SalInitInfoComponent implements OnInit {

  oggettoSAL;
  committenteSAL;
  impresaSAL;
  prezzoTotale;

  constructor(public userService: UserService) { }

  ngOnInit() {

    this.userService.getOggettoGDLInit().subscribe((res) => {
      this.oggettoSAL = res;
    },
      error => { this.oggettoSAL = error.error.text; });

    this.userService.getCommittenteGDLInit().subscribe((res) => {
      this.committenteSAL = res;
    },
      error => { this.committenteSAL = error.error.text; });

    this.userService.getImpresaGDLInit().subscribe((res) => {
      this.impresaSAL = res;
    },
      error => { this.impresaSAL = error.error.text; });

    this.userService.gettotalProgettoInit().subscribe((res) => {
      this.prezzoTotale = res;
    },
      error => { this.prezzoTotale = error.error.text; });
  }

}
