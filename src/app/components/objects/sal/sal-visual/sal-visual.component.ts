import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SalInitInfoComponent } from 'src/app/components/objects/sal/sal-init-info/sal-init-info.component';
import { MatDialog } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { UserService } from 'src/app/services/user/user.service';
import { SalInterface } from 'src/app/Interface/salInterface';
import { EmailComponent } from '../../email/email.component';
import { count } from 'rxjs/operators';
import { SalChartComponent } from '../sal-chart/sal-chart.component';

var SAL_DATA: SalInterface[];
const Sal = require('../../../../../../../sam_contracts/build/contracts/Sal.json');

@Component({
  selector: 'app-sal-visual',
  templateUrl: './sal-visual.component.html',
  styleUrls: ['./sal-visual.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed, void', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class SalVisualComponent implements OnInit {

  opened = false;
  state: string = 'default';

  data: any;
  salTab: any = [];
  mesi: any = [];
  debit: any = [];

  columnsToDisplay = ['Num', 'Data', 'Indicazioni', 'Informazioni'];
  dataSource = new MatTableDataSource(SAL_DATA);
  expandedElement: SalInterface | null;
  message: string;

  constructor(public dialog: MatDialog, public service: UserService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.addSal();
    this.service.getSal().subscribe((res) => {
      SAL_DATA = res;
      console.log(SAL_DATA);
      this.dataSource = new MatTableDataSource(SAL_DATA);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.addSalTable(SAL_DATA);
    }, error => console.log(error));

  }

  salInfoDialog() {
    this.dialog.open(SalInitInfoComponent);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  emailDialog() {
    this.dialog.open(EmailComponent);
  }

  addSalTable(DATA) {

    // conteggio degli slash nella data
    let contSlash = 0;

    // struttura da inserire nella tabella
    let temp = {};

    // indice della tabella dove inserie i dati
    let iter = 0;

    // Mese e anno da inserire nella nuova struttura dati, in modo da organizzare tutto per mese-anno
    let mouth;
    let year;

    // indice per il controllo della tabella, per verificare che due debiti appartengano allo stesso mese
    let control = 0;

    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < DATA.length; index++) {
      let date = DATA[index].data;
      const deb = parseInt(DATA[index].tot, 10);
      const indic = DATA[index].indicazione;
      // tslint:disable-next-line:prefer-for-of
      for (let ind = 0; ind < date.length; ind++) {
        const element = date[ind];
        if (element === "/" && contSlash === 0) {
          contSlash = contSlash + 1;
          mouth = date[ind + 1];
          if (date[ind + 2] !== "/") {
            mouth = mouth + date[ind + 2];
          }
        } else {
          if (element === "/" && contSlash === 1) {
            contSlash += 1;
            year = date[ind + 1] + date[ind + 2] + date[ind + 3] + date[ind + 4];
          }
        }
      }
      contSlash = 0;
      date += ':' + indic + ' ';
      temp = [{
        anno: year,
        mese: mouth,
        data: date,
        debito: deb
      }];
      if (iter === 0) {
        this.salTab[iter] = temp;
        iter++;
      } else {
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.salTab.length; i++) {
          if (this.salTab[i][0].mese === mouth && this.salTab[i][0].anno === year) {
            let val = parseInt(this.salTab[i][0].debito, 10);
            val += deb;
            this.salTab[i][0].debito = val;
            this.salTab[i][0].data += date;
            control += 1;
          }
        }
        if (control === 0) {
          this.salTab[iter] = temp;
          iter++;
        }
      }
      control = 0;
    }
    console.log(this.salTab);
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.salTab.length; i++) {
      this.debit[i] = this.salTab[i][0].debito;
      this.mesi[i] = this.salTab[i][0].mese;
      switch (this.mesi[i]) {
        case '1': {
          this.mesi[i] = 'Gennaio ' + this.salTab[i][0].anno;
          break;
        }
        case '2': {
          this.mesi[i] = 'Febbraio ' + this.salTab[i][0].anno;
          break;
        }
        case '3': {
          this.mesi[i] = 'Marzo ' + this.salTab[i][0].anno;
          break;
        }
        case '4': {
          this.mesi[i] = 'Aprile ' + this.salTab[i][0].anno;
          break;
        }
        case '5': {
          this.mesi[i] = 'Maggio ' + this.salTab[i][0].anno;
          break;
        }
        case '6': {
          this.mesi[i] = 'Giugno ' + this.salTab[i][0].anno;
          break;
        }
        case '7': {
          this.mesi[i] = 'Luglio ' + this.salTab[i][0].anno;
          break;
        }
        case '8': {
          this.mesi[i] = 'Agosto ' + this.salTab[i][0].anno;
          break;
        }
        case '9': {
          this.mesi[i] = 'Settembre ' + this.salTab[i][0].anno;
          break;
        }
        case '10': {
          this.mesi[i] = 'Ottobre ' + this.salTab[i][0].anno;
          break;
        }
        case '11': {
          this.mesi[i] = 'Novembre ' + this.salTab[i][0].anno;
          break;
        }
        case '12': {
          this.mesi[i] = 'Dicembre ' + this.salTab[i][0].anno;
          break;
        }
      }
    }
    console.log(this.mesi);
    console.log(this.debit);
    this.data = {
      datasets: [{
        label: 'Totale debito',
        data: this.debit,
        fill: false,
        backgroundColor: '#42A5F5',
        borderColor: '#1E88E5'
      }],
      labels: this.mesi,
    };
  }

  chartDialog() {
    this.dialog.open(SalChartComponent, {
      data: this.data
    });
  }

  async addSal() {
    const instanceSal = await this.service.getEventInstance(Sal);
    instanceSal.once('hackSetSal', {}, (err, event) => {
      this.service.getSal().subscribe((res) => {
        SAL_DATA = res;
        console.log(SAL_DATA);
        this.dataSource = new MatTableDataSource(SAL_DATA);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.addSalTable(SAL_DATA);
      }, error => console.log(error));
    });
  }

}
