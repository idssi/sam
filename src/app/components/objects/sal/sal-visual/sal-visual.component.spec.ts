import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalVisualComponent } from './sal-visual.component';

describe('SalVisualComponent', () => {
  let component: SalVisualComponent;
  let fixture: ComponentFixture<SalVisualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalVisualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalVisualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
