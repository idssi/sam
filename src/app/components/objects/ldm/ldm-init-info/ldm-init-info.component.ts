import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-ldm-init-info',
  templateUrl: './ldm-init-info.component.html',
  styleUrls: ['./ldm-init-info.component.css']
})
export class LdmInitInfoComponent implements OnInit {

  signRUP;

  oggettoLDM;
  committenteLDM;
  impresaLDM;
  prezzoTotale;
  constructor(public userService: UserService) { }

  ngOnInit() {


    this.userService.getOggettoGDLInit().subscribe((res) => {
      this.oggettoLDM = res;
    },
      error => { this.oggettoLDM = error.error.text; });

    this.userService.getCommittenteGDLInit().subscribe((res) => {
      this.committenteLDM = res;
    },
      error => { this.committenteLDM = error.error.text; });

    this.userService.getImpresaGDLInit().subscribe((res) => {
      this.impresaLDM = res;
    },
      error => { this.impresaLDM = error.error.text; });

    this.userService.gettotalProgettoInit().subscribe((res)=>{
      this.prezzoTotale = res;
    },
    error => { this.prezzoTotale = error.error.text; });
  }

}
