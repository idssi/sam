import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LdmInitInfoComponent } from './ldm-init-info.component';

describe('LdmInitInfoComponent', () => {
  let component: LdmInitInfoComponent;
  let fixture: ComponentFixture<LdmInitInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LdmInitInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LdmInitInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
