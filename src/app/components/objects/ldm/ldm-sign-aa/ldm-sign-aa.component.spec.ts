import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LdmSignAaComponent } from './ldm-sign-aa.component';

describe('LdmSignAaComponent', () => {
  let component: LdmSignAaComponent;
  let fixture: ComponentFixture<LdmSignAaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LdmSignAaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LdmSignAaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
