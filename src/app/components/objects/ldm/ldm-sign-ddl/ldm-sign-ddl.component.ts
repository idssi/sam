import { OnInit, Inject, Component } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { GdlInterface } from 'src/app/Interface/gdlInterface';
import { DlService } from 'src/app/services/dl/dl.service';
import { LoadingComponent } from '../../loading/loading.component';
export interface DialogData {
  misura: GdlInterface;
  id: number;
}

@Component({
  selector: 'app-ldm-sign-ddl',
  templateUrl: './ldm-sign-ddl.component.html',
  styleUrls: ['./ldm-sign-ddl.component.css']
})
export class LdmSignDdlComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<LdmSignDdlComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dlService: DlService, private dialog: MatDialog, private snack: MatSnackBar) {
  }

  ngOnInit() {
  }

  sign(id) {
    this.dlService.signMisuraDdl(id).then(res => {
      if (res['status'] === true) {
        this.dialog.closeAll();
        this.snack.open('Approvazione avvenuta con successo!', 'OK', {
          duration: 5000,
        });
      } else {
        this.dialog.closeAll();
        this.snack.open('Approvazione non avvenuta!', 'Errore', {
          duration: 5000,
        });
      }
    }).catch(error => {
      this.dialog.closeAll();
      this.snack.open('Approvazione non avvenuta!', 'Errore', {
        duration: 5000,
      });
    });
    this.dialog.open(LoadingComponent);
  }

  close() {
    this.dialog.closeAll();
  }

}
