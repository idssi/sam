import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LdmSignDdlComponent } from './ldm-sign-ddl.component';

describe('LdmSignDdlComponent', () => {
  let component: LdmSignDdlComponent;
  let fixture: ComponentFixture<LdmSignDdlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LdmSignDdlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LdmSignDdlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
