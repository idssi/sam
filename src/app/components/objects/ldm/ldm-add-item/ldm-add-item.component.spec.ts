import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LdmAddItemComponent } from './ldm-add-item.component';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

describe('LdmAddItemComponent', () => {
  let component: LdmAddItemComponent;
  let fixture: ComponentFixture<LdmAddItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LdmAddItemComponent ],
      imports: [
        MaterialModule,
        ReactiveFormsModule,
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LdmAddItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
