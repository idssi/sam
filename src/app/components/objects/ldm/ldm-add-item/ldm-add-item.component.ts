import { Component } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LdmInterface } from './../../../../Interface/ldmInterface';
import { MatDialog, MatSnackBar } from '@angular/material';
import { DlService } from 'src/app/services/dl/dl.service';
import { LoadingComponent } from '../../loading/loading.component';

@Component({
  selector: 'app-ldm-add-item',
  templateUrl: './ldm-add-item.component.html',
  styleUrls: ['./ldm-add-item.component.css']
})
export class LdmAddItemComponent {
  firstForm: FormGroup;
  secondForm: FormGroup;
  thirdForm: FormGroup;
  newMisureBook = new LdmInterface();
  minDate = new Date(2019, 0, 1);

  constructor(private formBuilder: FormBuilder, private dialog: MatDialog, private snack: MatSnackBar, public dlService: DlService) {
    this.firstForm = this.formBuilder.group({
      data: ['', [Validators.required]],
      tar: ['', [Validators.required]],
      des_lav: ['', [Validators.required]]
    });

    this.secondForm = this.formBuilder.group({
      costo: ['', [Validators.required, Validators.pattern("^[0-9]*$")]]
    });

    this.thirdForm = this.formBuilder.group({
      tot: ['', [Validators.required, Validators.pattern("^[0-9]*$")]]
    });
  }

  sendNewMisure() {
    this.newMisureBook.tar = this.firstForm.value.tar.toString();
    this.newMisureBook.data = this.firstForm.value.data.toLocaleDateString();
    this.newMisureBook.des_lav = this.firstForm.value.des_lav.toString();
    this.newMisureBook.costo = this.secondForm.value.costo.toString();
    this.newMisureBook.aliq = (((((this.secondForm.value.costo * this.thirdForm.value.tot) / 100) / 1000000) * 100).toString());
    this.newMisureBook.tot = this.thirdForm.value.tot.toString();

    console.log(this.newMisureBook);

    this.dlService.setMisura(this.newMisureBook).then(res => {
      console.log(res['status']);
      if (res['status'] === true) {
        this.dialog.closeAll();
        this.snack.open('Inserimento Avvenuto!', 'OK', {
          duration: 5000,
        });
      } else {
        this.dialog.closeAll();
        this.snack.open('inserimento non avvenuto!', 'Errore', {
          duration: 5000,
        });
      }
    }).catch(error => {
      this.dialog.closeAll();
      this.snack.open('inserimento non avvenuto!', 'Errore', {
        duration: 5000,
      });
    });
    this.dialog.open(LoadingComponent);
  }

}
