import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LdmVisualComponent } from './ldm-visual.component';

describe('LdmVisualComponent', () => {
  let component: LdmVisualComponent;
  let fixture: ComponentFixture<LdmVisualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LdmVisualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LdmVisualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
