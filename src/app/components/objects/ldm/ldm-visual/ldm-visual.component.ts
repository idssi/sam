import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LdmInitInfoComponent } from 'src/app/components/objects/ldm/ldm-init-info/ldm-init-info.component';
import { MatDialog } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { LdmInterface } from 'src/app/Interface/ldmInterface';
import { UserService } from 'src/app/services/user/user.service';
import { EmailComponent } from '../../email/email.component';
import { LdmSignDdlComponent } from '../ldm-sign-ddl/ldm-sign-ddl.component';
import { LdmSignAaComponent } from '../ldm-sign-aa/ldm-sign-aa.component';


const Ldm = require('../../../../../../../sam_contracts/build/contracts/Ldm.json');


var LDM_DATA: LdmInterface[]
@Component({
  selector: 'app-ldm-visual',
  templateUrl: './ldm-visual.component.html',
  styleUrls: ['./ldm-visual.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed, void', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})


export class LdmVisualComponent implements OnInit {

  opened = false;
  state: string = 'default';
  columnsToDisplay = ['Num', 'Data', 'DesLav', 'Informazioni'];
  dataSource = new MatTableDataSource(LDM_DATA);
  expandedElement: LdmInterface | null;
  message: string;

  constructor(public dialog: MatDialog, public service: UserService) { }


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.addLdm();
    this.signLdm();
    this.getLdm();
  }

  getLdm() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.service.getLdm().subscribe((res) => {
      LDM_DATA = res;
      this.dataSource = new MatTableDataSource(LDM_DATA);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, error => console.log(error));
  }

  ldmInfoDialog() {
    this.dialog.open(LdmInitInfoComponent);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  async addLdm() {
    const instanceLdm = await this.service.getEventInstance(Ldm);
    instanceLdm.once('hackSetMisura', {}, (error, event) => {
      console.log(event.returnValues);
      LDM_DATA.push({
        tar: event.returnValues[1][0],
        data: event.returnValues[1][1],
        des_lav: event.returnValues[1][2],
        costo: event.returnValues[1][3],
        aliq: event.returnValues[1][4],
        tot: event.returnValues[1][5],
        sign_ddl: event.returnValues[1][5],
        sign_aa: event.returnValues[1][5]
      });
      this.dataSource = new MatTableDataSource(LDM_DATA);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  async signLdm() {
    const instanceLdm = await this.service.getEventInstance(Ldm);
    instanceLdm.once('hackSignDdl', {}, (error, event) => {
      LDM_DATA[event.returnValues[2]].sign_ddl = 'true';
      this.dataSource = new MatTableDataSource(LDM_DATA);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    instanceLdm.once('hackSignAa', {}, (error, event) => {
      LDM_DATA[event.returnValues[2]].sign_aa = 'true';
      this.dataSource = new MatTableDataSource(LDM_DATA);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  emailDialog() {
    this.dialog.open(EmailComponent);
  }

  SignItemDdl(element, index) {
    this.dialog.open(LdmSignDdlComponent, {
      data: {misura: element, id: index}
    });
  }

  SignItemAa(element, index) {
    this.dialog.open(LdmSignAaComponent, {
      data: {misura: element, id: index}
    });
  }

  controlSign(approvato) {
    if (approvato === 'true') {
      return true;
    } else {
      return false;
    }
  }

  controlUsrAa() {
    if (window.location.pathname === '/aa') {
      return false;
    } else {
      return true;
    }
  }

  controlUsrDdl() {
    if (window.location.pathname === '/dl') {
      return false;
    } else {
      return true;
    }
  }

}