import { Component, OnInit, ViewChild } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatDialog, MatSnackBar } from '@angular/material';
import { GdlInterface } from '../../../Interface/gdlInterface';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DlService } from 'src/app/services/dl/dl.service';
import { EmailInterface } from 'src/app/Interface/emailInterface';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import {formatDate} from '@angular/common';


var EMAIL_DATA: EmailInterface[]

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed, void', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class EmailComponent implements OnInit {

  emailForm: FormGroup;
  newEmailInterface = new EmailInterface();

  opened = false;

  state: string = 'default';

  columnsToDisplay = ['Data', 'Oggetto', 'Mittente', 'Informazioni'];

  toppings = new FormControl();
  toppingList: string[] = ['Rup', 'Direttore', 'Impresa'];

  dataSource = new MatTableDataSource(EMAIL_DATA);
  expandedElement: GdlInterface | null;
  message: string;

  constructor(public dialog: MatDialog, public dlService: DlService,private snack: MatSnackBar, private formBuilder: FormBuilder) {


    this.emailForm = this.formBuilder.group({
      oggetto: ['', Validators.required],
      testo: ['', [Validators.required]]
    });

  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;




  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.dlService.getEmail().then((res) => {
      EMAIL_DATA = res;
      console.log(res);
      this.dataSource = new MatTableDataSource(EMAIL_DATA);
    }, error => console.log(error));



  }


  newEmail() {


    if (this.emailForm.dirty && this.emailForm.valid && this.toppings.valid) {
    
      console.log("Invio Valori");
      this.newEmailInterface.data = formatDate(new Date(), 'dd/MM/yyyy', 'en');
      this.newEmailInterface.destinatario = this.toppings.value;
      this.newEmailInterface.oggetto = this.emailForm.value.oggetto;
      this.newEmailInterface.testo = this.emailForm.value.testo;
  
  
      this.dlService.setEmail(this.newEmailInterface).then(res => {
        console.log(res['status']);
        if (res['status'] === true) {
         
          this.snack.open('Inserimento Avvenuto!', 'OK', {
            duration: 5000,
          });
        } else {
          
          this.snack.open('inserimento non avvenuto!', 'Errore', {
            duration: 5000,
          });
        }
      }).catch(error => {
       
        this.snack.open('inserimento non avvenuto!', 'Errore', {
          duration: 5000,
        });
      });
     
    }

    else {
      this.snack.open('Devi compilare tutti i campi!', 'Errore', {
        duration: 5000,
      });
    }
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  disableAnimation = true;
  ngAfterViewInit(): void {
    // timeout required to avoid the dreaded 'ExpressionChangedAfterItHasBeenCheckedError'
    setTimeout(() => this.disableAnimation = false);
  }

}




