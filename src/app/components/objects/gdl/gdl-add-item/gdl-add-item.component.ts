import { Component } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { GdlInterface } from './../../../../Interface/gdlInterface';
import { MatDialog, MatSnackBar } from '@angular/material';
import { DlService } from '../../../../services/dl/dl.service';
import { LoadingComponent } from '../../loading/loading.component';
import { GdlVisualComponent } from '../gdl-visual/gdl-visual.component';
import { RupService } from 'src/app/services/rup/rup.service';
import { AaService } from 'src/app/services/aa/aa.service';

@Component({
  selector: 'app-gdl-add-item',
  templateUrl: './gdl-add-item.component.html',
  styleUrls: ['./gdl-add-item.component.css'],
  providers: [GdlVisualComponent]
})
export class GdlAddItemComponent {
  firstForm: FormGroup;
  secondForm: FormGroup;
  thirdForm: FormGroup;
  newGdlJournal = new GdlInterface();
  minDate = new Date(2019, 0, 1);
  myService;
  constructor(private gdlV: GdlVisualComponent, private formBuilder: FormBuilder,
    private dialog: MatDialog, private snack: MatSnackBar,
    public dlService: DlService, public rupService: RupService, public aaService: AaService) {

    this.firstForm = this.formBuilder.group({
      meteo: ['', Validators.required],
      data: ['', [Validators.required]]
    });

    this.secondForm = this.formBuilder.group({
      annotazioni: ['', [Validators.required]]
    });

    this.thirdForm = this.formBuilder.group({
      operai: ['', [Validators.required, Validators.minLength(10)]],
      mezzi: ['', [Validators.required]]
    });

    switch(window.location.pathname) {
      case '/rup': {
        this.myService = this.rupService;
        break;
      }
      case '/dl': {
        this.myService = this.dlService;
        break;
      }
      case '/aa': {
        this.myService = this.aaService;
        break;
      }
    }
  }

  sendNewWork() {
    console.log("Invio Valori");
    this.newGdlJournal.meteo = this.firstForm.value.meteo;
    this.newGdlJournal.data = this.firstForm.value.data.toLocaleDateString();
    this.newGdlJournal.annotazioni = this.secondForm.value.annotazioni;
    this.newGdlJournal.operai = this.thirdForm.value.operai;
    this.newGdlJournal.mezzi = this.thirdForm.value.mezzi;
    this.newGdlJournal.path_allegato = "path";


    this.myService.setLavoro(this.newGdlJournal).then(res => {
      if (res['status'] === true) {
        this.dialog.closeAll();
        this.snack.open('Inserimento Avvenuto!', 'OK', {
          duration: 5000,
        });
      } else {
        this.dialog.closeAll();
        this.snack.open('inserimento non avvenuto!', 'Errore', {
          duration: 5000,
        });
      }
    }).catch(error => {
      this.dialog.closeAll();
      this.snack.open('inserimento non avvenuto!', 'Errore', {
        duration: 5000,
      });
    });
    this.dialog.open(LoadingComponent);
  }
}