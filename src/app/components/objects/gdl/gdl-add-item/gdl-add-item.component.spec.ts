import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GdlAddItemComponent } from './gdl-add-item.component';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

describe('GdlAddItemComponent', () => {
  let component: GdlAddItemComponent;
  let fixture: ComponentFixture<GdlAddItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GdlAddItemComponent ],
      imports: [
        MaterialModule,
        ReactiveFormsModule,
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GdlAddItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
