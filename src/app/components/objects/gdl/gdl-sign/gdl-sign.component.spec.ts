import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GdlSignComponent } from './gdl-sign.component';

describe('GdlSignComponent', () => {
  let component: GdlSignComponent;
  let fixture: ComponentFixture<GdlSignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GdlSignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GdlSignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
