import { OnInit, Inject, Component } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { GdlInterface } from 'src/app/Interface/gdlInterface';
import { DlService } from 'src/app/services/dl/dl.service';
import { LoadingComponent } from '../../loading/loading.component';
export interface DialogData {
  lavoro: GdlInterface;
  id: number;
}

@Component({
  selector: 'app-gdl-sign',
  templateUrl: './gdl-sign.component.html',
  styleUrls: ['./gdl-sign.component.css']
})
export class GdlSignComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<GdlSignComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dlService: DlService, private dialog: MatDialog, private snack: MatSnackBar) {
  }

  ngOnInit() {
  }

  sign(id) {
    console.log(id);
    this.dlService.approvaLavoro(id).then(res => {
      if (res['status'] === true) {
        this.dialog.closeAll();
        this.snack.open('Approvazione avvenuta con successo!', 'OK', {
          duration: 5000,
        });
      } else {
        this.dialog.closeAll();
        this.snack.open('Approvazione non avvenuta!', 'Errore', {
          duration: 5000,
        });
      }
    }).catch(error => {
      this.dialog.closeAll();
      this.snack.open('Approvazione non avvenuta!', 'Errore', {
        duration: 5000,
      });
    });
    this.dialog.open(LoadingComponent);
  }

  close() {
    this.dialog.closeAll();
  }

}
