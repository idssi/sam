import { Component, OnInit, ViewChild, Input, ChangeDetectorRef } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { GdlInitInfoComponent } from 'src/app/components/objects/gdl/gdl-init-info/gdl-init-info.component';
import { MatDialog } from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {UserService} from './../../../../services/user/user.service';
import {GdlInterface} from '../../../../Interface/gdlInterface';
import {EmailComponent} from '../../email/email.component';
import { getRenderedText } from '@angular/core/src/render3';
import { GdlSignComponent } from '../gdl-sign/gdl-sign.component';

const Gdl = require('../../../../../../../sam_contracts/build/contracts/Gdl.json');

var GDL_DATA: GdlInterface[];
@Component({
  selector: 'app-gdl-visual',
  templateUrl: './gdl-visual.component.html',
  styleUrls: ['./gdl-visual.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed, void', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class GdlVisualComponent implements OnInit {

  opened = false;
  state = 'default';

  columnsToDisplay = ['Num', 'Data', 'Annotazioni', 'Informazioni'];

  dataSource = new MatTableDataSource(GDL_DATA);
  expandedElement: GdlInterface | null;
  message: string;

  constructor(public dialog: MatDialog, public service: UserService) {}

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.visualGdl();
    this.addGdl();
    this.signGdl();
  }

  visualGdl() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.service.getGiornale().subscribe((res) => {
      GDL_DATA = res;
      this.dataSource = new MatTableDataSource(GDL_DATA);
      this.dataSource._updateChangeSubscription();
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, error => console.log(error));
  }

  async addGdl() {
    const instanceGdl = await this.service.getEventInstance(Gdl);
    instanceGdl.once('hackSetLavoro', {}, (error, event) => {
      GDL_DATA.push({
        data: event.returnValues[1][0],
        meteo: event.returnValues[1][1],
        annotazioni: event.returnValues[1][2],
        operai: event.returnValues[1][3],
        mezzi: event.returnValues[1][4],
        path_allegato: event.returnValues[1][5],
        approvato: 'false'
      });
      this.dataSource = new MatTableDataSource(GDL_DATA);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  async signGdl() {
    const instanceGdl = await this.service.getEventInstance(Gdl);
    instanceGdl.once('hackSign', {}, (error, event) => {
      GDL_DATA[event.returnValues[2]].approvato = 'true';
      this.dataSource = new MatTableDataSource(GDL_DATA);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  gdlInfoDialog() {
    this.dialog.open(GdlInitInfoComponent);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  emailDialog() {
    this.dialog.open(EmailComponent);
  }

  gdlSignItem(element, index) {
    this.dialog.open(GdlSignComponent, {
      data: {lavoro: element, id: index}
    });
  }

  controlSign(approvato) {
    if (approvato === 'true') {
      return true;
    } else {
      return false;}
  }

}
