import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GdlVisualComponent } from './gdl-visual.component';

describe('GdlVisualComponent', () => {
  let component: GdlVisualComponent;
  let fixture: ComponentFixture<GdlVisualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GdlVisualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GdlVisualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
