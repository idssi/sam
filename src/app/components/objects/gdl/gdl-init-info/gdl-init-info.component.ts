import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-gdl-init-info',
  templateUrl: './gdl-init-info.component.html',
  styleUrls: ['./gdl-init-info.component.css']
})
export class GdlInitInfoComponent implements OnInit {

  signDL;
  signRUP;
  signAA;
  oggettoGDL;
  committenteGDL;
  impresaGDL;

  constructor(public userService: UserService) { }

  async ngOnInit() {


    this.userService.getOggettoGDLInit().subscribe((res) => {
      this.oggettoGDL = res;
    },
      error => { this.oggettoGDL = error.error.text; });

    this.userService.getCommittenteGDLInit().subscribe((res) => {
      this.committenteGDL = res;
    },
      error => { this.committenteGDL = error.error.text; });

    this.userService.getImpresaGDLInit().subscribe((res) => {
      this.impresaGDL = res;
    },
      error => { this.impresaGDL = error.error.text; });

      
  }

}
