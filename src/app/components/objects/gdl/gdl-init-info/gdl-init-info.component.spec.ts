import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GdlInitInfoComponent } from './gdl-init-info.component';

describe('GdlInitInfoComponent', () => {
  let component: GdlInitInfoComponent;
  let fixture: ComponentFixture<GdlInitInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GdlInitInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GdlInitInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
