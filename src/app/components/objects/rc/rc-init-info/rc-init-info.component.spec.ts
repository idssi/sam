import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RcInitInfoComponent } from './rc-init-info.component';

describe('RcInitInfoComponent', () => {
  let component: RcInitInfoComponent;
  let fixture: ComponentFixture<RcInitInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RcInitInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RcInitInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
