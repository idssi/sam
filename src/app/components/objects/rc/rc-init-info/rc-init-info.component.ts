import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-rc-init-info',
  templateUrl: './rc-init-info.component.html',
  styleUrls: ['./rc-init-info.component.css']
})
export class RcInitInfoComponent implements OnInit {

  signRUP;
  oggettoRC;
  committenteRC;
  impresaRC;
  prezzoTotale;

  constructor(public userService: UserService) { }

  ngOnInit() {


    this.userService.getOggettoGDLInit().subscribe((res) => {
      this.oggettoRC = res;
    },
      error => { this.oggettoRC = error.error.text; });

    this.userService.getCommittenteGDLInit().subscribe((res) => {
      this.committenteRC = res;
    },
      error => { this.committenteRC = error.error.text; });

    this.userService.getImpresaGDLInit().subscribe((res) => {
      this.impresaRC = res;
    },
      error => { this.impresaRC = error.error.text; });

    this.userService.gettotalProgettoInit().subscribe((res) => {
      this.prezzoTotale = res;
    },
      error => { this.prezzoTotale = error.error.text; });
  }


}
