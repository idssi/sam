import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RcVisualComponent } from './rc-visual.component';

describe('RcVisualComponent', () => {
  let component: RcVisualComponent;
  let fixture: ComponentFixture<RcVisualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RcVisualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RcVisualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
