import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { RcInitInfoComponent } from 'src/app/components/objects/rc/rc-init-info/rc-init-info.component';
import { MatDialog } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { RcInterface } from 'src/app/Interface/rcInterface';
import { UserService } from 'src/app/services/user/user.service';
import { EmailComponent } from '../../email/email.component';
import { RcSignAaComponent } from '../rc-sign-aa/rc-sign-aa.component';
import { RcSignDdlComponent } from '../rc-sign-ddl/rc-sign-ddl.component';

const Rc = require('../../../../../../../sam_contracts/build/contracts/Rc.json');


var RC_DATA: RcInterface[]
@Component({
  selector: 'app-rc-visual',
  templateUrl: './rc-visual.component.html',
  styleUrls: ['./rc-visual.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed, void', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class RcVisualComponent implements OnInit {

  opened = false;
  state: string = 'default';


  columnsToDisplay = ['Num', 'Data', 'Indicazioni', 'Informazioni'];
  dataSource = new MatTableDataSource(RC_DATA);
  expandedElement: RcInterface | null;
  message: string;


  constructor(public dialog: MatDialog, public service: UserService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.signRc();
    this.addRc();
    this.getRc();
  }

  getRc() {
    this.service.getRegistri().subscribe((res) => {
      RC_DATA = res;
      this.dataSource = new MatTableDataSource(RC_DATA);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, error => console.log(error));

  }

  rcInfoDialog() {
    this.dialog.open(RcInitInfoComponent);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  emailDialog() {
    this.dialog.open(EmailComponent);
  }

  async addRc() {
    const instanceRc = await this.service.getEventInstance(Rc);
    instanceRc.once('hackSetRegistro', {}, (error, event) => {
      RC_DATA.push({
        tariffa: event.returnValues[1].tariffa,
        data: event.returnValues[1].data,
        indicazione: event.returnValues[1].indicazione,
        debito: event.returnValues[1].debito,
        aliq: event.returnValues[1].aliq,
        tot: event.returnValues[1].tot,
        sign_DDL: event.returnValues[1].sign_DDL,
        sign_AA: event.returnValues[1].sign_AA,
        prez_unit: event.returnValues[1].prezzoUnitario,
        percentuale: event.returnValues[1].percentuale
      });
      this.dataSource = new MatTableDataSource(RC_DATA);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  async signRc() {
    const instanceRc = await this.service.getEventInstance(Rc);
    instanceRc.once('hackSignDdl', {}, (error, event) => {
      RC_DATA[parseInt(event.returnValues[2], 10)]['signDDL'] = 'true';
      this.dataSource = new MatTableDataSource(RC_DATA);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    instanceRc.once('hackSignAa', {}, (error, event) => {
      RC_DATA[parseInt(event.returnValues[2], 10)]['signAA'] = 'true';
      this.dataSource = new MatTableDataSource(RC_DATA);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  SignItemDdl(element, index) {
    this.dialog.open(RcSignDdlComponent , {
      data: {voce: element, id: index}
    });
  }

  SignItemAa(element, index) {
    this.dialog.open(RcSignAaComponent , {
      data: {voce: element, id: index}
    });
  }

  controlSign(approvato) {
    if (approvato === 'true') {
      return true;
    } else {
      return false;
    }
  }

  controlUsrAa() {
    if (window.location.pathname === '/aa') {
      return false;
    } else {
      return true;
    }
  }

  controlUsrDdl() {
    if (window.location.pathname === '/dl') {
      return false;
    } else {
      return true;
    }
  }

}
