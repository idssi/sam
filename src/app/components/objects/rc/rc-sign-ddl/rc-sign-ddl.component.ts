import { OnInit, Inject, Component } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { GdlInterface } from 'src/app/Interface/gdlInterface';
import { LoadingComponent } from '../../loading/loading.component';
import { DlService } from 'src/app/services/dl/dl.service';
export interface DialogData {
  voce: GdlInterface;
  id: number;
}

@Component({
  selector: 'app-rc-sign-ddl',
  templateUrl: './rc-sign-ddl.component.html',
  styleUrls: ['./rc-sign-ddl.component.css']
})
export class RcSignDdlComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<RcSignDdlComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dlService: DlService, private dialog: MatDialog, private snack: MatSnackBar) {
  }

  ngOnInit() {
  }

  sign(id) {
    this.dlService.approvaRegistroDDL(id).then(res => {
      if (res['status'] === true) {
        this.dialog.closeAll();
        this.snack.open('Approvazione avvenuta con successo!', 'OK', {
          duration: 5000,
        });
      } else {
        this.dialog.closeAll();
        this.snack.open('Approvazione non avvenuta!', 'Errore', {
          duration: 5000,
        });
      }
    }).catch(error => {
      this.dialog.closeAll();
      this.snack.open('Approvazione non avvenuta!', 'Errore', {
        duration: 5000,
      });
    });
    this.dialog.open(LoadingComponent);
  }

  close() {
    this.dialog.closeAll();
  }

}
