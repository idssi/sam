import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RcSignDdlComponent } from './rc-sign-ddl.component';

describe('RcSignDdlComponent', () => {
  let component: RcSignDdlComponent;
  let fixture: ComponentFixture<RcSignDdlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RcSignDdlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RcSignDdlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
