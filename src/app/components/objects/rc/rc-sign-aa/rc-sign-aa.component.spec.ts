import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RcSignAaComponent } from './rc-sign-aa.component';

describe('RcSignAaComponent', () => {
  let component: RcSignAaComponent;
  let fixture: ComponentFixture<RcSignAaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RcSignAaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RcSignAaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
