import { OnInit, Inject, Component } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { GdlInterface } from 'src/app/Interface/gdlInterface';
import { LoadingComponent } from '../../loading/loading.component';
import { AaService } from 'src/app/services/aa/aa.service';
export interface DialogData {
  voce: GdlInterface;
  id: number;
}

@Component({
  selector: 'app-rc-sign-aa',
  templateUrl: './rc-sign-aa.component.html',
  styleUrls: ['./rc-sign-aa.component.css']
})
export class RcSignAaComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<RcSignAaComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public aAService: AaService, private dialog: MatDialog, private snack: MatSnackBar) {
  }

  ngOnInit() {
  }

  sign(id) {
    console.log(id);
    this.aAService.approvaRegistroAA(id).then(res => {
      if (res['status'] === true) {
        this.dialog.closeAll();
        this.snack.open('Approvazione avvenuta con successo!', 'OK', {
          duration: 5000,
        });
      } else {
        this.dialog.closeAll();
        this.snack.open('Approvazione non avvenuta!', 'Errore', {
          duration: 5000,
        });
      }
    }).catch(error => {
      this.dialog.closeAll();
      this.snack.open('Approvazione non avvenuta!', 'Errore', {
        duration: 5000,
      });
    });
    this.dialog.open(LoadingComponent);
  }

  close() {
    this.dialog.closeAll();
  }

}
