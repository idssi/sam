import { Component} from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { RcInterface } from './../../../../Interface/rcInterface';
import { MatDialog, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-rc-add-item',
  templateUrl: './rc-add-item.component.html',
  styleUrls: ['./rc-add-item.component.css']
})
export class RcAddItemComponent {
  firstForm: FormGroup;
  secondForm: FormGroup;
  thirdForm: FormGroup;
  fourthForm: FormGroup;
  newRegistryBook = new RcInterface();

  constructor(private formBuilder: FormBuilder, private dialog: MatDialog, private snack: MatSnackBar) {
    this.firstForm = this.formBuilder.group({
      data: ['', [Validators.required]],
      lavoro: ['', [Validators.required]],
      libretto: ['', [Validators.required]]
    });

    this.secondForm = this.formBuilder.group({
      tariffa: ['', [Validators.required, Validators.pattern("^[0-9]*$")]]
    });

    this.thirdForm = this.formBuilder.group({
      prezzo: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      quantita: ['', [Validators.required, Validators.pattern("^[0-9]*$")]]
    });

    this.fourthForm = this.formBuilder.group({
      importo: ['', [Validators.required, Validators.pattern("^[0-9]*$")]]
    });
  }

  sendNewRegistry() {
    this.newRegistryBook.tariffa = this.firstForm.value.tariffa;
    this.newRegistryBook.data = this.firstForm.value.data;
    this.newRegistryBook.indicazione = this.firstForm.value.indicazione;
    // this.newRegistryBook.totaleProg = this.firstForm.value.totaleProg;
    this.newRegistryBook.percentuale = this.firstForm.value.percentuale;
    this.newRegistryBook.prez_unit = this.firstForm.value.prezzoUnitario;
    this.newRegistryBook.aliq = this.firstForm.value.aliq;
    this.newRegistryBook.debito = this.firstForm.value.debito;
    this.newRegistryBook.tot = this.firstForm.value.tot;
    this.dialog.closeAll();
    this.snack.open("Inserimento Avvenuto!", "OK", {
      duration: 5000,
    });
  }


}
