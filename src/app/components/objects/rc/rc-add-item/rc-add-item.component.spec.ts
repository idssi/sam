import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { RcAddItemComponent } from './rc-add-item.component';

describe('RcAddItemComponent', () => {
  let component: RcAddItemComponent;
  let fixture: ComponentFixture<RcAddItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RcAddItemComponent ],
      imports: [
        MaterialModule,
        ReactiveFormsModule,
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RcAddItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
