import { Component } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {


  constructor(public auth: AuthService, public router: Router) {
    auth.handleAuthentication();
    // Attempt single sign-on authentication
    // if not authenticated
    if (auth.isAuthenticated()) {
    } else {
      auth.renewTokens();
    }
    // Hide the overlay when single sign-on auth is complete
    auth.ssoAuthComplete$.subscribe(
      status => {
        console.log(this.auth.decAccessToken);
      }
    );
  }
}
