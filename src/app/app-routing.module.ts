import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicHomeComponent } from './components/public/public-home/public-home.component';
import { DlHomeComponent } from './components/subjects/dl/dl-home/dl-home.component';
import { RupHomeComponent } from './components/subjects/rup/rup-home/rup-home.component';
import { AaHomeComponent } from './components/subjects/aa/aa-home/aa-home.component';
import { CallbackComponent } from './components/_plus/callback/callback.component';
import { UserHomeComponent } from './components/subjects/user/user-home/user-home.component';

const routes: Routes = [
  {
    path: '',
    component: PublicHomeComponent
  },
  {
    path: 'dl',
    component: DlHomeComponent
  },
  {
    path: 'rup',
    component: RupHomeComponent
  },
  {
    path: 'aa',
    component: AaHomeComponent
  },
  {
    path: 'user',
    component: UserHomeComponent
  },
  {
    path: 'callback',
    component: CallbackComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
